#!/usr/bin/env bash

set -ue -o pipefail -o posix

main() {
    if [[ -n "$PIPELINE_IMAGE_PLATFORMS" ]]; then
        multi_platform_build_and_push
    else
        standard_build_and_push
    fi
}

standard_build_and_push() {
    ensure_build_command_exists
    run_build_command
    ensure_image_was_built
    push_image
}

ensure_image_was_built() {
    echo "Looking for IMAGE_NAME=$IMAGE_NAME"
    if ! docker inspect "$IMAGE_NAME" &> /dev/null; then
        echo "WARNING: Image not found. No image will be pushed."
        exit 0
    fi
}

push_image() {
    authenticate_with_registry
    docker tag "$IMAGE_NAME" "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
    docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
}

multi_platform_build_and_push() {
    echo "Performing a multi-platform build!"
    ensure_build_command_exists
    define_PIPELINE_IMAGE_NAME
    define_PIPELINE_BUILDX_BUILD_OPTIONS
    create_and_use_buildx_builder
    authenticate_with_registry
    run_build_command
}

define_PIPELINE_IMAGE_NAME() {
    local image="$CI_REGISTRY_IMAGE"    # <registry_uri>/<name>
    local branch="$CI_COMMIT_REF_SLUG"

    PIPELINE_IMAGE_NAME="$image:$branch"
    export PIPELINE_IMAGE_NAME
}

define_PIPELINE_BUILDX_BUILD_OPTIONS() {
    PIPELINE_BUILDX_BUILD_OPTIONS=""

    # Pull the most recent images that we depend on.
    PIPELINE_BUILDX_BUILD_OPTIONS="$PIPELINE_BUILDX_BUILD_OPTIONS --pull"

    # Push new images after they are built.
    PIPELINE_BUILDX_BUILD_OPTIONS="$PIPELINE_BUILDX_BUILD_OPTIONS --push"

    # TODO: In the PIPEILNE_BUILDX_OPTIONS definition below,
    # --provenance=false disables security features in buildx
    # that are currently (02-27-2023) incompatible with GitLab.
    # Keep an eye on https://gitlab.com/gitlab-org/gitlab/-/issues/388865 .
    # When it is fixed, we can and should re-enable this feature.
    PIPELINE_BUILDX_BUILD_OPTIONS="$PIPELINE_BUILDX_BUILD_OPTIONS --provenance=false"

    # Specify the platforms to build.
    if [[ -n "$PIPELINE_IMAGE_PLATFORMS" ]] ; then
        PIPELINE_BUILDX_BUILD_OPTIONS="$PIPELINE_BUILDX_BUILD_OPTIONS --platform=${PIPELINE_IMAGE_PLATFORMS}"
    fi

    # Set the image name/location.
    PIPELINE_BUILDX_BUILD_OPTIONS="$PIPELINE_BUILDX_BUILD_OPTIONS --tag $PIPELINE_IMAGE_NAME"
}


create_and_use_buildx_builder() {
    docker buildx create --use
}

authenticate_with_registry() {
    docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
}

ensure_build_command_exists() {
    if [ -z "$BUILD_COMMAND" ]; then
        echo "ERROR: BUILD_COMMAND is not defined."
        exit 1
    fi
}

run_build_command() {
    echo "$BUILD_COMMAND"
    $BUILD_COMMAND
}

main
